"use strict";

var items = [];

// $('body').find('button').click(function(e){
// 	var btn = $(e.target);
// 	if(btn.name === "The Force Awakens"){
// 		alert("Woohoo");
// 	} else {
// 		alert("OMG");
// 	}
// });

function getThings() {
	$.ajax({
		url: "http://swapi.co/api/films/",
		dataType: "json"
	}).done(function (data) {
		processData(data);
	});

	function processData(data) {
		$.each(data, function (index, value) {
			if (index === "results") {
				for (var item in index) {
					items.push(data[index][item]);
				}
			}
		});
	}

	countFilms();
}

function countFilms() {
	for (var i = items.length - 1; i >= 0; i--) {
		console.log(items[i]);
		// var container =
		var container = $(".printbox");
		var box = $("<div>");
		box.addClass("movieDiv");
		box.attr("id", "movieDiv" + i);
		box.append("<h1>" + items[i].title + "</h1>");
		box.append("<strong>" + items[i].director + "</strong>");
		box.append("<p>" + items[i].opening_crawl + "</p>");
		// box.append("<p>" + items[i].characters + "</p>");

		var searchBtn = document.createElement("input");
		//Set the attributes
		searchBtn.setAttribute("onclick", "getActors(this.name)");
		searchBtn.setAttribute("type", "button");
		searchBtn.setAttribute("value", "Search");
		searchBtn.setAttribute("id", "sw" + i + "");
		searchBtn.setAttribute("name", items[i].url);

		// btn.onclick = getActors;

		box.append(searchBtn);
		container.append(box);
	}
}

function getActors(name) {
	// console.log("clicked");
	var parentTitle;
	var parent;
	$.ajax({
		url: name,
		dataType: "json"
	}).done(function (data) {
		parentTitle = $("h1:contains(" + data.title + ")").css("text-decoration", "underline");
		parent = parentTitle.parent();
		console.log(parent);
		for (var i = data.characters.length - 1; i >= 0; i--) {
			$.ajax({
				url: data.characters[i],
				dataType: "json"
			}).done(function (character) {
				// console.log(character.name);
				parent.append("<p>" + character.name + "</p>");
			});
		}
	});

	for (var i = items.length - 1; i >= 0; i--) {
		var box = $("<div>");

		var ul = $("<ul>");
		for (var b = items[i].characters.length - 1; b >= 0; b--) {
			ul.append("<li>" + items[i].characters[b] + "</li>");
		}
		box.append(ul);
	}
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImluZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7O0FBQUEsSUFBSSxRQUFRLEVBQVo7Ozs7Ozs7Ozs7O0FBV0EsU0FBUyxTQUFULEdBQW9CO0FBQ25CLEdBQUUsSUFBRixDQUFPO0FBQ04sT0FBSyw0QkFEQztBQUVOLFlBQVU7QUFGSixFQUFQLEVBR0csSUFISCxDQUdRLFVBQVMsSUFBVCxFQUFjO0FBQ3JCLGNBQVksSUFBWjtBQUNBLEVBTEQ7O0FBT0EsVUFBUyxXQUFULENBQXFCLElBQXJCLEVBQTBCO0FBQ3pCLElBQUUsSUFBRixDQUFPLElBQVAsRUFBYSxVQUFTLEtBQVQsRUFBZ0IsS0FBaEIsRUFBc0I7QUFDbEMsT0FBRyxVQUFVLFNBQWIsRUFBdUI7QUFDdEIsU0FBSSxJQUFJLElBQVIsSUFBZ0IsS0FBaEIsRUFBc0I7QUFDckIsV0FBTSxJQUFOLENBQVcsS0FBSyxLQUFMLEVBQVksSUFBWixDQUFYO0FBQ0E7QUFDRDtBQUNELEdBTkQ7QUFPQTs7QUFFRDtBQUNBOztBQUVELFNBQVMsVUFBVCxHQUFxQjtBQUNwQixNQUFLLElBQUksSUFBSSxNQUFNLE1BQU4sR0FBZSxDQUE1QixFQUErQixLQUFLLENBQXBDLEVBQXVDLEdBQXZDLEVBQTRDO0FBQzNDLFVBQVEsR0FBUixDQUFZLE1BQU0sQ0FBTixDQUFaOztBQUVBLE1BQUksWUFBWSxFQUFFLFdBQUYsQ0FBaEI7QUFDQSxNQUFJLE1BQU0sRUFBRSxPQUFGLENBQVY7QUFDQSxNQUFJLFFBQUosQ0FBYSxVQUFiO0FBQ0EsTUFBSSxJQUFKLENBQVMsSUFBVCxFQUFjLGFBQVcsQ0FBekI7QUFDQSxNQUFJLE1BQUosQ0FBVyxTQUFTLE1BQU0sQ0FBTixFQUFTLEtBQWxCLEdBQTBCLE9BQXJDO0FBQ0EsTUFBSSxNQUFKLENBQVcsYUFBYSxNQUFNLENBQU4sRUFBUyxRQUF0QixHQUFpQyxXQUE1QztBQUNBLE1BQUksTUFBSixDQUFXLFFBQVEsTUFBTSxDQUFOLEVBQVMsYUFBakIsR0FBaUMsTUFBNUM7OztBQUdBLE1BQUksWUFBWSxTQUFTLGFBQVQsQ0FBdUIsT0FBdkIsQ0FBaEI7O0FBRUssWUFBVSxZQUFWLENBQXVCLFNBQXZCLEVBQWlDLHNCQUFqQztBQUNBLFlBQVUsWUFBVixDQUF1QixNQUF2QixFQUE4QixRQUE5QjtBQUNBLFlBQVUsWUFBVixDQUF1QixPQUF2QixFQUErQixRQUEvQjtBQUNBLFlBQVUsWUFBVixDQUF1QixJQUF2QixFQUE0QixPQUFNLENBQU4sR0FBUyxFQUFyQztBQUNBLFlBQVUsWUFBVixDQUF1QixNQUF2QixFQUE4QixNQUFNLENBQU4sRUFBUyxHQUF2Qzs7OztBQUlMLE1BQUksTUFBSixDQUFXLFNBQVg7QUFDQSxZQUFVLE1BQVYsQ0FBaUIsR0FBakI7QUFDQTtBQUNEOztBQUlELFNBQVMsU0FBVCxDQUFtQixJQUFuQixFQUF3Qjs7QUFFdkIsS0FBSSxXQUFKO0FBQ0EsS0FBSSxNQUFKO0FBQ0EsR0FBRSxJQUFGLENBQU87QUFDTixPQUFLLElBREM7QUFFTixZQUFVO0FBRkosRUFBUCxFQUdHLElBSEgsQ0FHUSxVQUFTLElBQVQsRUFBYztBQUNyQixnQkFBYyxFQUFFLGlCQUFlLEtBQUssS0FBcEIsR0FBMEIsR0FBNUIsRUFBaUMsR0FBakMsQ0FBcUMsaUJBQXJDLEVBQXdELFdBQXhELENBQWQ7QUFDQSxXQUFTLFlBQVksTUFBWixFQUFUO0FBQ0EsVUFBUSxHQUFSLENBQVksTUFBWjtBQUNBLE9BQUssSUFBSSxJQUFJLEtBQUssVUFBTCxDQUFnQixNQUFoQixHQUF5QixDQUF0QyxFQUF5QyxLQUFLLENBQTlDLEVBQWlELEdBQWpELEVBQXNEO0FBQ3JELEtBQUUsSUFBRixDQUFPO0FBQ04sU0FBSyxLQUFLLFVBQUwsQ0FBZ0IsQ0FBaEIsQ0FEQztBQUVOLGNBQVU7QUFGSixJQUFQLEVBR0csSUFISCxDQUdRLFVBQVMsU0FBVCxFQUFtQjs7QUFFMUIsV0FBTyxNQUFQLENBQWMsUUFBUSxVQUFVLElBQWxCLEdBQXlCLE1BQXZDO0FBQ0EsSUFORDtBQU9BO0FBQ0QsRUFoQkQ7O0FBbUJBLE1BQUssSUFBSSxJQUFJLE1BQU0sTUFBTixHQUFlLENBQTVCLEVBQStCLEtBQUssQ0FBcEMsRUFBdUMsR0FBdkMsRUFBNEM7QUFDM0MsTUFBSSxNQUFNLEVBQUUsT0FBRixDQUFWOztBQUVBLE1BQUksS0FBSyxFQUFFLE1BQUYsQ0FBVDtBQUNBLE9BQUssSUFBSSxJQUFJLE1BQU0sQ0FBTixFQUFTLFVBQVQsQ0FBb0IsTUFBcEIsR0FBNkIsQ0FBMUMsRUFBNkMsS0FBSyxDQUFsRCxFQUFxRCxHQUFyRCxFQUEwRDtBQUN6RCxNQUFHLE1BQUgsQ0FBVSxTQUFTLE1BQU0sQ0FBTixFQUFTLFVBQVQsQ0FBb0IsQ0FBcEIsQ0FBVCxHQUFrQyxPQUE1QztBQUNBO0FBQ0QsTUFBSSxNQUFKLENBQVcsRUFBWDtBQUNBO0FBQ0QiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyJ2YXIgaXRlbXMgPSBbXTtcblxuLy8gJCgnYm9keScpLmZpbmQoJ2J1dHRvbicpLmNsaWNrKGZ1bmN0aW9uKGUpe1xuLy8gXHR2YXIgYnRuID0gJChlLnRhcmdldCk7XG4vLyBcdGlmKGJ0bi5uYW1lID09PSBcIlRoZSBGb3JjZSBBd2FrZW5zXCIpe1xuLy8gXHRcdGFsZXJ0KFwiV29vaG9vXCIpO1xuLy8gXHR9IGVsc2Uge1xuLy8gXHRcdGFsZXJ0KFwiT01HXCIpO1xuLy8gXHR9XG4vLyB9KTtcblxuZnVuY3Rpb24gZ2V0VGhpbmdzKCl7XG5cdCQuYWpheCh7XG5cdFx0dXJsOiBcImh0dHA6Ly9zd2FwaS5jby9hcGkvZmlsbXMvXCIsXG5cdFx0ZGF0YVR5cGU6IFwianNvblwiXG5cdH0pLmRvbmUoZnVuY3Rpb24oZGF0YSl7XG5cdFx0cHJvY2Vzc0RhdGEoZGF0YSk7XG5cdH0pO1xuXG5cdGZ1bmN0aW9uIHByb2Nlc3NEYXRhKGRhdGEpe1xuXHRcdCQuZWFjaChkYXRhLCBmdW5jdGlvbihpbmRleCwgdmFsdWUpe1xuXHRcdFx0aWYoaW5kZXggPT09IFwicmVzdWx0c1wiKXtcblx0XHRcdFx0Zm9yKHZhciBpdGVtIGluIGluZGV4KXtcblx0XHRcdFx0XHRpdGVtcy5wdXNoKGRhdGFbaW5kZXhdW2l0ZW1dKTtcblx0XHRcdFx0fVxuXHRcdFx0fVxuXHRcdH0pO1xuXHR9XG5cblx0Y291bnRGaWxtcygpO1xufVxuXG5mdW5jdGlvbiBjb3VudEZpbG1zKCl7XG5cdGZvciAodmFyIGkgPSBpdGVtcy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuXHRcdGNvbnNvbGUubG9nKGl0ZW1zW2ldKTtcblx0XHQvLyB2YXIgY29udGFpbmVyID0gXG5cdFx0dmFyIGNvbnRhaW5lciA9ICQoXCIucHJpbnRib3hcIik7XG5cdFx0dmFyIGJveCA9ICQoXCI8ZGl2PlwiKTtcblx0XHRib3guYWRkQ2xhc3MoXCJtb3ZpZURpdlwiKTtcblx0XHRib3guYXR0cihcImlkXCIsXCJtb3ZpZURpdlwiK2kpO1xuXHRcdGJveC5hcHBlbmQoXCI8aDE+XCIgKyBpdGVtc1tpXS50aXRsZSArIFwiPC9oMT5cIik7XG5cdFx0Ym94LmFwcGVuZChcIjxzdHJvbmc+XCIgKyBpdGVtc1tpXS5kaXJlY3RvciArIFwiPC9zdHJvbmc+XCIpO1xuXHRcdGJveC5hcHBlbmQoXCI8cD5cIiArIGl0ZW1zW2ldLm9wZW5pbmdfY3Jhd2wgKyBcIjwvcD5cIik7XG5cdFx0Ly8gYm94LmFwcGVuZChcIjxwPlwiICsgaXRlbXNbaV0uY2hhcmFjdGVycyArIFwiPC9wPlwiKTtcblxuXHRcdHZhciBzZWFyY2hCdG4gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KFwiaW5wdXRcIik7XG4gICAgICAgLy9TZXQgdGhlIGF0dHJpYnV0ZXNcbiAgICAgICBzZWFyY2hCdG4uc2V0QXR0cmlidXRlKFwib25jbGlja1wiLFwiZ2V0QWN0b3JzKHRoaXMubmFtZSlcIik7XG4gICAgICAgc2VhcmNoQnRuLnNldEF0dHJpYnV0ZShcInR5cGVcIixcImJ1dHRvblwiKTtcbiAgICAgICBzZWFyY2hCdG4uc2V0QXR0cmlidXRlKFwidmFsdWVcIixcIlNlYXJjaFwiKTtcbiAgICAgICBzZWFyY2hCdG4uc2V0QXR0cmlidXRlKFwiaWRcIixcInN3XCIrIGkgK1wiXCIpO1xuICAgICAgIHNlYXJjaEJ0bi5zZXRBdHRyaWJ1dGUoXCJuYW1lXCIsaXRlbXNbaV0udXJsKTtcblxuXHRcdC8vIGJ0bi5vbmNsaWNrID0gZ2V0QWN0b3JzO1xuXG5cdFx0Ym94LmFwcGVuZChzZWFyY2hCdG4pO1xuXHRcdGNvbnRhaW5lci5hcHBlbmQoYm94KTtcblx0fVxufVxuXG5cblxuZnVuY3Rpb24gZ2V0QWN0b3JzKG5hbWUpe1xuXHQvLyBjb25zb2xlLmxvZyhcImNsaWNrZWRcIik7XG5cdHZhciBwYXJlbnRUaXRsZTtcblx0dmFyIHBhcmVudDtcblx0JC5hamF4KHtcblx0XHR1cmw6IG5hbWUsXG5cdFx0ZGF0YVR5cGU6IFwianNvblwiXG5cdH0pLmRvbmUoZnVuY3Rpb24oZGF0YSl7XG5cdFx0cGFyZW50VGl0bGUgPSAkKFwiaDE6Y29udGFpbnMoXCIrZGF0YS50aXRsZStcIilcIikuY3NzKFwidGV4dC1kZWNvcmF0aW9uXCIsIFwidW5kZXJsaW5lXCIpO1xuXHRcdHBhcmVudCA9IHBhcmVudFRpdGxlLnBhcmVudCgpO1xuXHRcdGNvbnNvbGUubG9nKHBhcmVudCk7XG5cdFx0Zm9yICh2YXIgaSA9IGRhdGEuY2hhcmFjdGVycy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuXHRcdFx0JC5hamF4KHtcblx0XHRcdFx0dXJsOiBkYXRhLmNoYXJhY3RlcnNbaV0sXG5cdFx0XHRcdGRhdGFUeXBlOiBcImpzb25cIlxuXHRcdFx0fSkuZG9uZShmdW5jdGlvbihjaGFyYWN0ZXIpe1xuXHRcdFx0XHQvLyBjb25zb2xlLmxvZyhjaGFyYWN0ZXIubmFtZSk7XG5cdFx0XHRcdHBhcmVudC5hcHBlbmQoXCI8cD5cIiArIGNoYXJhY3Rlci5uYW1lICsgXCI8L3A+XCIpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHR9KTtcblxuXG5cdGZvciAodmFyIGkgPSBpdGVtcy5sZW5ndGggLSAxOyBpID49IDA7IGktLSkge1xuXHRcdHZhciBib3ggPSAkKFwiPGRpdj5cIik7XG5cblx0XHR2YXIgdWwgPSAkKFwiPHVsPlwiKTtcblx0XHRmb3IgKHZhciBiID0gaXRlbXNbaV0uY2hhcmFjdGVycy5sZW5ndGggLSAxOyBiID49IDA7IGItLSkge1xuXHRcdFx0dWwuYXBwZW5kKFwiPGxpPlwiICsgaXRlbXNbaV0uY2hhcmFjdGVyc1tiXSArIFwiPC9saT5cIik7XG5cdFx0fVxuXHRcdGJveC5hcHBlbmQodWwpO1xuXHR9XG59Il19
