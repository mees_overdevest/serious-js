'use strict';

//Global variables
var movieData = void 0;
var movieGallery = void 0;

/**
 * Initialize the application (after DOM ready)
 */
var init = function init() {
    getAllMovies();
    movieGallery = document.getElementById('movies');
    movieGallery.addEventListener('click', movieCardClickHandler);
};

/**
 * Get all the Star Wars movies
 */
var getAllMovies = function getAllMovies() {
    ajaxRequest("http://swapi.co/api/films/", showMovies);
};

/**
 * Generic AJAX handler (to prevent reqwest everywhere)
 *
 * @param url
 * @param ajaxSuccessHandler
 * @param [data]
 */
var ajaxRequest = function ajaxRequest(url, ajaxSuccessHandler, data) {
    //Default ajax parameters
    var parameters = {
        type: 'json',
        url: url,
        success: ajaxSuccessHandler
    };

    //If data is passed, add it to the AJAX parameters
    if (data) {
        parameters.data = data;
    }

    //Actual AJAX call
    reqwest(parameters);
};

/**
 * Process the AJAX response from the server to show the movies
 *
 * @param data
 */
var showMovies = function showMovies(data) {
    //Store in global for later use
    movieData = data.results;
    var cardNumber = 0;

    movieData.map(function (movie) {
        var card = createDomElement({ tagName: 'div', attributes: { class: 'card c' + cardNumber } });

        //Create all children HTML tags for the card
        var title = createDomElement({ tagName: 'h2', attributes: { class: 'title' }, content: movie.title });
        var subTitleString = movie.director + "  (" + movie.release_date + ")";
        var subTitle = createDomElement({ tagName: 'h3', attributes: { class: 'subTitle' }, content: subTitleString });
        var intro = createDomElement({ tagName: 'p', attributes: { class: 'intro' }, content: movie.opening_crawl });
        var actorLink = createDomElement({ tagName: 'a', attributes: { class: 'load-actors show-actors', href: '#', id: 'card-' + cardNumber }, content: 'Show actors' });

        //Append all elements to the card
        card.appendChild(title);
        card.appendChild(subTitle);
        card.appendChild(intro);
        card.appendChild(actorLink);

        //Append the card to the gallery
        movieGallery.appendChild(card);
        cardNumber++;
    });
};

/**
 * Handle the click on the card
 *
 * @param e
 */
var movieCardClickHandler = function movieCardClickHandler(e) {
    //Prevent default behavior & save target in local variable
    e.preventDefault();
    var target = e.target;

    //If element does not have the class card, exit this function
    if (!target.classList.contains('load-actors')) {
        return;
    }

    //Remove class to prevent more clicks on this item
    target.classList.remove('load-actors');

    //Create a list for an item
    var list = createDomElement({ tagName: 'ul', attributes: { class: 'actors' } });
    target.parentElement.appendChild(list);

    //Get the movie from the global stored data
    var index = parseInt(target.id.replace('card-', ''));
    var movie = movieData[index];

    //Load every character that's present in the movie
    for (var i = 0; i < movie.characters.length; i++) {
        ajaxRequest(movie.characters[i], showCharactersByMovie.bind(undefined, list));
    }
};

/**
 * Process the AJAX response from the server to show the characters
 *
 * @param list
 * @param data
 */
var showCharactersByMovie = function showCharactersByMovie(list, data) {
    //Create list item for every
    var li = createDomElement({ tagName: 'li', attributes: { class: 'actor' }, content: data.name });
    list.appendChild(li);
};

/**
 * Generic function to create new DOM elements
 *
 * @param properties
 * @returns {Element}
 */
var createDomElement = function createDomElement(properties) {
    //Create the element
    var domElement = document.createElement(properties.tagName);

    //Loop through the attributes to set them on the element
    var attributes = properties.attributes;
    for (var prop in attributes) {
        domElement.setAttribute(prop, attributes[prop]);
    }

    //If any content, set the inner HTML
    if (properties.content) {
        domElement.innerHTML = properties.content;
    }

    //Return to use in other functions
    return domElement;
};

//Actual DOM ready handler
window.addEventListener("load", init);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm1haW4uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7O0FBQ0EsSUFBSSxrQkFBSjtBQUNBLElBQUkscUJBQUo7Ozs7O0FBS0EsSUFBSSxPQUFPLFNBQVAsSUFBTyxHQUNYO0FBQ0k7QUFDQSxtQkFBZSxTQUFTLGNBQVQsQ0FBd0IsUUFBeEIsQ0FBZjtBQUNBLGlCQUFhLGdCQUFiLENBQThCLE9BQTlCLEVBQXVDLHFCQUF2QztBQUNILENBTEQ7Ozs7O0FBVUEsSUFBSSxlQUFlLFNBQWYsWUFBZSxHQUNuQjtBQUNJLGdCQUFZLDRCQUFaLEVBQTBDLFVBQTFDO0FBQ0gsQ0FIRDs7Ozs7Ozs7O0FBWUEsSUFBSSxjQUFjLFNBQWQsV0FBYyxDQUFDLEdBQUQsRUFBTSxrQkFBTixFQUEwQixJQUExQixFQUNsQjs7QUFFSSxRQUFJLGFBQWE7QUFDYixjQUFNLE1BRE87QUFFYixhQUFLLEdBRlE7QUFHYixpQkFBUztBQUhJLEtBQWpCOzs7QUFPQSxRQUFJLElBQUosRUFBVTtBQUNOLG1CQUFXLElBQVgsR0FBa0IsSUFBbEI7QUFDSDs7O0FBR0QsWUFBUSxVQUFSO0FBQ0gsQ0FoQkQ7Ozs7Ozs7QUF1QkEsSUFBSSxhQUFhLFNBQWIsVUFBYSxDQUFDLElBQUQsRUFDakI7O0FBRUksZ0JBQVksS0FBSyxPQUFqQjtBQUNBLFFBQUksYUFBYSxDQUFqQjs7QUFFQSxjQUFVLEdBQVYsQ0FBYyxVQUFDLEtBQUQsRUFDZDtBQUNJLFlBQUksT0FBTyxpQkFBaUIsRUFBQyxTQUFTLEtBQVYsRUFBaUIsWUFBWSxFQUFDLE9BQU8sV0FBVyxVQUFuQixFQUE3QixFQUFqQixDQUFYOzs7QUFHQSxZQUFJLFFBQVEsaUJBQWlCLEVBQUMsU0FBUyxJQUFWLEVBQWdCLFlBQVksRUFBQyxPQUFPLE9BQVIsRUFBNUIsRUFBOEMsU0FBUyxNQUFNLEtBQTdELEVBQWpCLENBQVo7QUFDQSxZQUFJLGlCQUFpQixNQUFNLFFBQU4sR0FBaUIsS0FBakIsR0FBeUIsTUFBTSxZQUEvQixHQUE4QyxHQUFuRTtBQUNBLFlBQUksV0FBVyxpQkFBaUIsRUFBQyxTQUFTLElBQVYsRUFBZ0IsWUFBWSxFQUFDLE9BQU8sVUFBUixFQUE1QixFQUFpRCxTQUFTLGNBQTFELEVBQWpCLENBQWY7QUFDQSxZQUFJLFFBQVEsaUJBQWlCLEVBQUMsU0FBUyxHQUFWLEVBQWUsWUFBWSxFQUFDLE9BQU8sT0FBUixFQUEzQixFQUE2QyxTQUFTLE1BQU0sYUFBNUQsRUFBakIsQ0FBWjtBQUNBLFlBQUksWUFBWSxpQkFBaUIsRUFBQyxTQUFTLEdBQVYsRUFBZSxZQUFZLEVBQUMsT0FBTyx5QkFBUixFQUFtQyxNQUFNLEdBQXpDLEVBQThDLElBQUksVUFBVSxVQUE1RCxFQUEzQixFQUFvRyxTQUFTLGFBQTdHLEVBQWpCLENBQWhCOzs7QUFHQSxhQUFLLFdBQUwsQ0FBaUIsS0FBakI7QUFDQSxhQUFLLFdBQUwsQ0FBaUIsUUFBakI7QUFDQSxhQUFLLFdBQUwsQ0FBaUIsS0FBakI7QUFDQSxhQUFLLFdBQUwsQ0FBaUIsU0FBakI7OztBQUdBLHFCQUFhLFdBQWIsQ0FBeUIsSUFBekI7QUFDQTtBQUNILEtBcEJEO0FBcUJILENBM0JEOzs7Ozs7O0FBa0NBLElBQUksd0JBQXdCLFNBQXhCLHFCQUF3QixDQUFDLENBQUQsRUFDNUI7O0FBRUksTUFBRSxjQUFGO0FBQ0EsUUFBSSxTQUFTLEVBQUUsTUFBZjs7O0FBR0EsUUFBSSxDQUFDLE9BQU8sU0FBUCxDQUFpQixRQUFqQixDQUEwQixhQUExQixDQUFMLEVBQStDO0FBQzNDO0FBQ0g7OztBQUdELFdBQU8sU0FBUCxDQUFpQixNQUFqQixDQUF3QixhQUF4Qjs7O0FBR0EsUUFBSSxPQUFPLGlCQUFpQixFQUFDLFNBQVMsSUFBVixFQUFnQixZQUFZLEVBQUMsT0FBTyxRQUFSLEVBQTVCLEVBQWpCLENBQVg7QUFDQSxXQUFPLGFBQVAsQ0FBcUIsV0FBckIsQ0FBaUMsSUFBakM7OztBQUdBLFFBQUksUUFBUSxTQUFTLE9BQU8sRUFBUCxDQUFVLE9BQVYsQ0FBa0IsT0FBbEIsRUFBMkIsRUFBM0IsQ0FBVCxDQUFaO0FBQ0EsUUFBSSxRQUFRLFVBQVUsS0FBVixDQUFaOzs7QUFHQSxTQUFLLElBQUksSUFBSSxDQUFiLEVBQWdCLElBQUksTUFBTSxVQUFOLENBQWlCLE1BQXJDLEVBQTZDLEdBQTdDLEVBQWtEO0FBQzlDLG9CQUFZLE1BQU0sVUFBTixDQUFpQixDQUFqQixDQUFaLEVBQWlDLHNCQUFzQixJQUF0QixZQUFpQyxJQUFqQyxDQUFqQztBQUNIO0FBQ0osQ0ExQkQ7Ozs7Ozs7O0FBa0NBLElBQUksd0JBQXdCLFNBQXhCLHFCQUF3QixDQUFDLElBQUQsRUFBTyxJQUFQLEVBQzVCOztBQUVJLFFBQUksS0FBSyxpQkFBaUIsRUFBQyxTQUFTLElBQVYsRUFBZ0IsWUFBWSxFQUFDLE9BQU8sT0FBUixFQUE1QixFQUE4QyxTQUFTLEtBQUssSUFBNUQsRUFBakIsQ0FBVDtBQUNBLFNBQUssV0FBTCxDQUFpQixFQUFqQjtBQUNILENBTEQ7Ozs7Ozs7O0FBYUEsSUFBSSxtQkFBbUIsU0FBbkIsZ0JBQW1CLENBQUMsVUFBRCxFQUN2Qjs7QUFFSSxRQUFJLGFBQWEsU0FBUyxhQUFULENBQXVCLFdBQVcsT0FBbEMsQ0FBakI7OztBQUdBLFFBQUksYUFBYSxXQUFXLFVBQTVCO0FBQ0EsU0FBSyxJQUFJLElBQVQsSUFBaUIsVUFBakIsRUFBNkI7QUFDekIsbUJBQVcsWUFBWCxDQUF3QixJQUF4QixFQUE4QixXQUFXLElBQVgsQ0FBOUI7QUFDSDs7O0FBR0QsUUFBSSxXQUFXLE9BQWYsRUFBd0I7QUFDcEIsbUJBQVcsU0FBWCxHQUF1QixXQUFXLE9BQWxDO0FBQ0g7OztBQUdELFdBQU8sVUFBUDtBQUNILENBbEJEOzs7QUFxQkEsT0FBTyxnQkFBUCxDQUF3QixNQUF4QixFQUFnQyxJQUFoQyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy9HbG9iYWwgdmFyaWFibGVzXG5sZXQgbW92aWVEYXRhO1xubGV0IG1vdmllR2FsbGVyeTtcblxuLyoqXG4gKiBJbml0aWFsaXplIHRoZSBhcHBsaWNhdGlvbiAoYWZ0ZXIgRE9NIHJlYWR5KVxuICovXG52YXIgaW5pdCA9ICgpID0+XG57XG4gICAgZ2V0QWxsTW92aWVzKCk7XG4gICAgbW92aWVHYWxsZXJ5ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ21vdmllcycpO1xuICAgIG1vdmllR2FsbGVyeS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIG1vdmllQ2FyZENsaWNrSGFuZGxlcik7XG59O1xuXG4vKipcbiAqIEdldCBhbGwgdGhlIFN0YXIgV2FycyBtb3ZpZXNcbiAqL1xudmFyIGdldEFsbE1vdmllcyA9ICgpID0+XG57XG4gICAgYWpheFJlcXVlc3QoXCJodHRwOi8vc3dhcGkuY28vYXBpL2ZpbG1zL1wiLCBzaG93TW92aWVzKTtcbn07XG5cbi8qKlxuICogR2VuZXJpYyBBSkFYIGhhbmRsZXIgKHRvIHByZXZlbnQgcmVxd2VzdCBldmVyeXdoZXJlKVxuICpcbiAqIEBwYXJhbSB1cmxcbiAqIEBwYXJhbSBhamF4U3VjY2Vzc0hhbmRsZXJcbiAqIEBwYXJhbSBbZGF0YV1cbiAqL1xudmFyIGFqYXhSZXF1ZXN0ID0gKHVybCwgYWpheFN1Y2Nlc3NIYW5kbGVyLCBkYXRhKSA9Plxue1xuICAgIC8vRGVmYXVsdCBhamF4IHBhcmFtZXRlcnNcbiAgICBsZXQgcGFyYW1ldGVycyA9IHtcbiAgICAgICAgdHlwZTogJ2pzb24nLFxuICAgICAgICB1cmw6IHVybCxcbiAgICAgICAgc3VjY2VzczogYWpheFN1Y2Nlc3NIYW5kbGVyXG4gICAgfTtcblxuICAgIC8vSWYgZGF0YSBpcyBwYXNzZWQsIGFkZCBpdCB0byB0aGUgQUpBWCBwYXJhbWV0ZXJzXG4gICAgaWYgKGRhdGEpIHtcbiAgICAgICAgcGFyYW1ldGVycy5kYXRhID0gZGF0YTtcbiAgICB9XG5cbiAgICAvL0FjdHVhbCBBSkFYIGNhbGxcbiAgICByZXF3ZXN0KHBhcmFtZXRlcnMpO1xufTtcblxuLyoqXG4gKiBQcm9jZXNzIHRoZSBBSkFYIHJlc3BvbnNlIGZyb20gdGhlIHNlcnZlciB0byBzaG93IHRoZSBtb3ZpZXNcbiAqXG4gKiBAcGFyYW0gZGF0YVxuICovXG52YXIgc2hvd01vdmllcyA9IChkYXRhKSA9Plxue1xuICAgIC8vU3RvcmUgaW4gZ2xvYmFsIGZvciBsYXRlciB1c2VcbiAgICBtb3ZpZURhdGEgPSBkYXRhLnJlc3VsdHM7XG4gICAgbGV0IGNhcmROdW1iZXIgPSAwO1xuXG4gICAgbW92aWVEYXRhLm1hcCgobW92aWUpID0+XG4gICAge1xuICAgICAgICBsZXQgY2FyZCA9IGNyZWF0ZURvbUVsZW1lbnQoe3RhZ05hbWU6ICdkaXYnLCBhdHRyaWJ1dGVzOiB7Y2xhc3M6ICdjYXJkIGMnICsgY2FyZE51bWJlcn19KTtcblxuICAgICAgICAvL0NyZWF0ZSBhbGwgY2hpbGRyZW4gSFRNTCB0YWdzIGZvciB0aGUgY2FyZFxuICAgICAgICBsZXQgdGl0bGUgPSBjcmVhdGVEb21FbGVtZW50KHt0YWdOYW1lOiAnaDInLCBhdHRyaWJ1dGVzOiB7Y2xhc3M6ICd0aXRsZSd9LCBjb250ZW50OiBtb3ZpZS50aXRsZX0pO1xuICAgICAgICBsZXQgc3ViVGl0bGVTdHJpbmcgPSBtb3ZpZS5kaXJlY3RvciArIFwiICAoXCIgKyBtb3ZpZS5yZWxlYXNlX2RhdGUgKyBcIilcIjtcbiAgICAgICAgbGV0IHN1YlRpdGxlID0gY3JlYXRlRG9tRWxlbWVudCh7dGFnTmFtZTogJ2gzJywgYXR0cmlidXRlczoge2NsYXNzOiAnc3ViVGl0bGUnfSwgY29udGVudDogc3ViVGl0bGVTdHJpbmd9KTtcbiAgICAgICAgbGV0IGludHJvID0gY3JlYXRlRG9tRWxlbWVudCh7dGFnTmFtZTogJ3AnLCBhdHRyaWJ1dGVzOiB7Y2xhc3M6ICdpbnRybyd9LCBjb250ZW50OiBtb3ZpZS5vcGVuaW5nX2NyYXdsfSk7XG4gICAgICAgIGxldCBhY3RvckxpbmsgPSBjcmVhdGVEb21FbGVtZW50KHt0YWdOYW1lOiAnYScsIGF0dHJpYnV0ZXM6IHtjbGFzczogJ2xvYWQtYWN0b3JzIHNob3ctYWN0b3JzJywgaHJlZjogJyMnLCBpZDogJ2NhcmQtJyArIGNhcmROdW1iZXJ9LCBjb250ZW50OiAnU2hvdyBhY3RvcnMnfSk7XG5cbiAgICAgICAgLy9BcHBlbmQgYWxsIGVsZW1lbnRzIHRvIHRoZSBjYXJkXG4gICAgICAgIGNhcmQuYXBwZW5kQ2hpbGQodGl0bGUpO1xuICAgICAgICBjYXJkLmFwcGVuZENoaWxkKHN1YlRpdGxlKTtcbiAgICAgICAgY2FyZC5hcHBlbmRDaGlsZChpbnRybyk7XG4gICAgICAgIGNhcmQuYXBwZW5kQ2hpbGQoYWN0b3JMaW5rKTtcblxuICAgICAgICAvL0FwcGVuZCB0aGUgY2FyZCB0byB0aGUgZ2FsbGVyeVxuICAgICAgICBtb3ZpZUdhbGxlcnkuYXBwZW5kQ2hpbGQoY2FyZCk7XG4gICAgICAgIGNhcmROdW1iZXIrKztcbiAgICB9KTtcbn07XG5cbi8qKlxuICogSGFuZGxlIHRoZSBjbGljayBvbiB0aGUgY2FyZFxuICpcbiAqIEBwYXJhbSBlXG4gKi9cbnZhciBtb3ZpZUNhcmRDbGlja0hhbmRsZXIgPSAoZSkgPT5cbntcbiAgICAvL1ByZXZlbnQgZGVmYXVsdCBiZWhhdmlvciAmIHNhdmUgdGFyZ2V0IGluIGxvY2FsIHZhcmlhYmxlXG4gICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgIGxldCB0YXJnZXQgPSBlLnRhcmdldDtcblxuICAgIC8vSWYgZWxlbWVudCBkb2VzIG5vdCBoYXZlIHRoZSBjbGFzcyBjYXJkLCBleGl0IHRoaXMgZnVuY3Rpb25cbiAgICBpZiAoIXRhcmdldC5jbGFzc0xpc3QuY29udGFpbnMoJ2xvYWQtYWN0b3JzJykpIHtcbiAgICAgICAgcmV0dXJuO1xuICAgIH1cblxuICAgIC8vUmVtb3ZlIGNsYXNzIHRvIHByZXZlbnQgbW9yZSBjbGlja3Mgb24gdGhpcyBpdGVtXG4gICAgdGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoJ2xvYWQtYWN0b3JzJyk7XG5cbiAgICAvL0NyZWF0ZSBhIGxpc3QgZm9yIGFuIGl0ZW1cbiAgICBsZXQgbGlzdCA9IGNyZWF0ZURvbUVsZW1lbnQoe3RhZ05hbWU6ICd1bCcsIGF0dHJpYnV0ZXM6IHtjbGFzczogJ2FjdG9ycyd9fSk7XG4gICAgdGFyZ2V0LnBhcmVudEVsZW1lbnQuYXBwZW5kQ2hpbGQobGlzdCk7XG5cbiAgICAvL0dldCB0aGUgbW92aWUgZnJvbSB0aGUgZ2xvYmFsIHN0b3JlZCBkYXRhXG4gICAgbGV0IGluZGV4ID0gcGFyc2VJbnQodGFyZ2V0LmlkLnJlcGxhY2UoJ2NhcmQtJywgJycpKTtcbiAgICBsZXQgbW92aWUgPSBtb3ZpZURhdGFbaW5kZXhdO1xuXG4gICAgLy9Mb2FkIGV2ZXJ5IGNoYXJhY3RlciB0aGF0J3MgcHJlc2VudCBpbiB0aGUgbW92aWVcbiAgICBmb3IgKGxldCBpID0gMDsgaSA8IG1vdmllLmNoYXJhY3RlcnMubGVuZ3RoOyBpKyspIHtcbiAgICAgICAgYWpheFJlcXVlc3QobW92aWUuY2hhcmFjdGVyc1tpXSwgc2hvd0NoYXJhY3RlcnNCeU1vdmllLmJpbmQodGhpcywgbGlzdCkpO1xuICAgIH1cbn07XG5cbi8qKlxuICogUHJvY2VzcyB0aGUgQUpBWCByZXNwb25zZSBmcm9tIHRoZSBzZXJ2ZXIgdG8gc2hvdyB0aGUgY2hhcmFjdGVyc1xuICpcbiAqIEBwYXJhbSBsaXN0XG4gKiBAcGFyYW0gZGF0YVxuICovXG52YXIgc2hvd0NoYXJhY3RlcnNCeU1vdmllID0gKGxpc3QsIGRhdGEpID0+XG57XG4gICAgLy9DcmVhdGUgbGlzdCBpdGVtIGZvciBldmVyeVxuICAgIGxldCBsaSA9IGNyZWF0ZURvbUVsZW1lbnQoe3RhZ05hbWU6ICdsaScsIGF0dHJpYnV0ZXM6IHtjbGFzczogJ2FjdG9yJ30sIGNvbnRlbnQ6IGRhdGEubmFtZX0pO1xuICAgIGxpc3QuYXBwZW5kQ2hpbGQobGkpO1xufTtcblxuLyoqXG4gKiBHZW5lcmljIGZ1bmN0aW9uIHRvIGNyZWF0ZSBuZXcgRE9NIGVsZW1lbnRzXG4gKlxuICogQHBhcmFtIHByb3BlcnRpZXNcbiAqIEByZXR1cm5zIHtFbGVtZW50fVxuICovXG52YXIgY3JlYXRlRG9tRWxlbWVudCA9IChwcm9wZXJ0aWVzKSA9Plxue1xuICAgIC8vQ3JlYXRlIHRoZSBlbGVtZW50XG4gICAgbGV0IGRvbUVsZW1lbnQgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KHByb3BlcnRpZXMudGFnTmFtZSk7XG5cbiAgICAvL0xvb3AgdGhyb3VnaCB0aGUgYXR0cmlidXRlcyB0byBzZXQgdGhlbSBvbiB0aGUgZWxlbWVudFxuICAgIGxldCBhdHRyaWJ1dGVzID0gcHJvcGVydGllcy5hdHRyaWJ1dGVzO1xuICAgIGZvciAobGV0IHByb3AgaW4gYXR0cmlidXRlcykge1xuICAgICAgICBkb21FbGVtZW50LnNldEF0dHJpYnV0ZShwcm9wLCBhdHRyaWJ1dGVzW3Byb3BdKTtcbiAgICB9XG5cbiAgICAvL0lmIGFueSBjb250ZW50LCBzZXQgdGhlIGlubmVyIEhUTUxcbiAgICBpZiAocHJvcGVydGllcy5jb250ZW50KSB7XG4gICAgICAgIGRvbUVsZW1lbnQuaW5uZXJIVE1MID0gcHJvcGVydGllcy5jb250ZW50O1xuICAgIH1cblxuICAgIC8vUmV0dXJuIHRvIHVzZSBpbiBvdGhlciBmdW5jdGlvbnNcbiAgICByZXR1cm4gZG9tRWxlbWVudDtcbn07XG5cbi8vQWN0dWFsIERPTSByZWFkeSBoYW5kbGVyXG53aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcImxvYWRcIiwgaW5pdCk7XG4iXX0=
