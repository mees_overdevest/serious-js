//Global variables
let movieData;
let movieGallery;

/**
 * Initialize the application (after DOM ready)
 */
var init = () =>
{
    getAllMovies();
    movieGallery = document.getElementById('movies');
    movieGallery.addEventListener('click', movieCardClickHandler);
};

/**
 * Get all the Star Wars movies
 */
var getAllMovies = () =>
{
    ajaxRequest("http://swapi.co/api/films/", showMovies);
};

/**
 * Generic AJAX handler (to prevent reqwest everywhere)
 *
 * @param url
 * @param ajaxSuccessHandler
 * @param [data]
 */
var ajaxRequest = (url, ajaxSuccessHandler, data) =>
{
    //Default ajax parameters
    let parameters = {
        type: 'json',
        url: url,
        success: ajaxSuccessHandler
    };

    //If data is passed, add it to the AJAX parameters
    if (data) {
        parameters.data = data;
    }

    //Actual AJAX call
    reqwest(parameters);
};

/**
 * Process the AJAX response from the server to show the movies
 *
 * @param data
 */
var showMovies = (data) =>
{
    //Store in global for later use
    movieData = data.results;
    let cardNumber = 0;

    movieData.map((movie) =>
    {
        let card = createDomElement({tagName: 'div', attributes: {class: 'card c' + cardNumber}});

        //Create all children HTML tags for the card
        let title = createDomElement({tagName: 'h2', attributes: {class: 'title'}, content: movie.title});
        let subTitleString = movie.director + "  (" + movie.release_date + ")";
        let subTitle = createDomElement({tagName: 'h3', attributes: {class: 'subTitle'}, content: subTitleString});
        let intro = createDomElement({tagName: 'p', attributes: {class: 'intro'}, content: movie.opening_crawl});
        let actorLink = createDomElement({tagName: 'a', attributes: {class: 'load-actors show-actors', href: '#', id: 'card-' + cardNumber}, content: 'Show actors'});

        //Append all elements to the card
        card.appendChild(title);
        card.appendChild(subTitle);
        card.appendChild(intro);
        card.appendChild(actorLink);

        //Append the card to the gallery
        movieGallery.appendChild(card);
        cardNumber++;
    });
};

/**
 * Handle the click on the card
 *
 * @param e
 */
var movieCardClickHandler = (e) =>
{
    //Prevent default behavior & save target in local variable
    e.preventDefault();
    let target = e.target;

    //If element does not have the class card, exit this function
    if (!target.classList.contains('load-actors')) {
        return;
    }

    //Remove class to prevent more clicks on this item
    target.classList.remove('load-actors');

    //Create a list for an item
    let list = createDomElement({tagName: 'ul', attributes: {class: 'actors'}});
    target.parentElement.appendChild(list);

    //Get the movie from the global stored data
    let index = parseInt(target.id.replace('card-', ''));
    let movie = movieData[index];

    //Load every character that's present in the movie
    for (let i = 0; i < movie.characters.length; i++) {
        ajaxRequest(movie.characters[i], showCharactersByMovie.bind(this, list));
    }
};

/**
 * Process the AJAX response from the server to show the characters
 *
 * @param list
 * @param data
 */
var showCharactersByMovie = (list, data) =>
{
    //Create list item for every
    let li = createDomElement({tagName: 'li', attributes: {class: 'actor'}, content: data.name});
    list.appendChild(li);
};

/**
 * Generic function to create new DOM elements
 *
 * @param properties
 * @returns {Element}
 */
var createDomElement = (properties) =>
{
    //Create the element
    let domElement = document.createElement(properties.tagName);

    //Loop through the attributes to set them on the element
    let attributes = properties.attributes;
    for (let prop in attributes) {
        domElement.setAttribute(prop, attributes[prop]);
    }

    //If any content, set the inner HTML
    if (properties.content) {
        domElement.innerHTML = properties.content;
    }

    //Return to use in other functions
    return domElement;
};

//Actual DOM ready handler
window.addEventListener("load", init);
