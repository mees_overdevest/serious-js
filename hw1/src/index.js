var items = [];

// $('body').find('button').click(function(e){
// 	var btn = $(e.target);
// 	if(btn.name === "The Force Awakens"){
// 		alert("Woohoo");
// 	} else {
// 		alert("OMG");
// 	}
// });

function getThings(){
	$.ajax({
		url: "http://swapi.co/api/films/",
		dataType: "json"
	}).done(function(data){
		processData(data);
	});

	function processData(data){
		$.each(data, function(index, value){
			if(index === "results"){
				for(var item in index){
					items.push(data[index][item]);
				}
			}
		});
	}

	appendFilms();
}

function appendFilms(){
	for (var i = items.length - 1; i >= 0; i--) {
		console.log(items[i]);
		// var container = 
		var container = $(".printbox");
		var box = $("<div>");
		box.addClass("movieDiv");
		box.attr("id","movieDiv"+i);
		box.append("<h1>" + items[i].title + "</h1>");
		box.append("<strong>" + items[i].director + "</strong>");
		box.append("<p>" + items[i].opening_crawl + "</p>");
		// box.append("<p>" + items[i].characters + "</p>");

		var searchBtn = document.createElement("input");
       //Set the attributes
       searchBtn.setAttribute("onclick","getActors(this.name)");
       searchBtn.setAttribute("type","button");
       searchBtn.setAttribute("value","Search");
       searchBtn.setAttribute("id","sw"+ i +"");
       searchBtn.setAttribute("name",items[i].url);

		// btn.onclick = getActors;

		box.append(searchBtn);
		container.append(box);
	}
}



function getActors(name){
	// console.log("clicked");
	var parentTitle;
	var parent;
	$.ajax({
		url: name,
		dataType: "json"
	}).done(function(data){
		parentTitle = $("h1:contains("+data.title+")").css("text-decoration", "underline");
		parent = parentTitle.parent();
		console.log(parent);
		for (var i = data.characters.length - 1; i >= 0; i--) {
			$.ajax({
				url: data.characters[i],
				dataType: "json"
			}).done(function(character){
				// console.log(character.name);
				parent.append("<p>" + character.name + "</p>");
			});
		}
	});


	for (var i = items.length - 1; i >= 0; i--) {
		var box = $("<div>");

		var ul = $("<ul>");
		for (var b = items[i].characters.length - 1; b >= 0; b--) {
			ul.append("<li>" + items[i].characters[b] + "</li>");
		}
		box.append(ul);
	}
}