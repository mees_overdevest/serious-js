/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';

	// Simple AJAX request

	function getThings(link) {
		$.ajax({
			url: link,
			dataType: "json"
		}).done(function (data) {
			return console.log(data);
		});
	}

	// Json requests for arrays or single URL's
	var fetchJSON = function fetchJSON(url) {
		if (url.constructor === Array) {
			for (var i = url.length - 1; i >= 0; i--) {
				return new Promise(function (resolve, reject) {
					$.getJSON(url[i]).done(function (json) {
						return resolve(json);
					}).fail(function (xhr, status, err) {
						return reject(status + err.message);
					});
				});
			}
		} else {
			return new Promise(function (resolve, reject) {
				$.getJSON(url).done(function (json) {
					return resolve(json);
				}).fail(function (xhr, status, err) {
					return reject(status + err.message);
				});
			});
		}
	};

	var links = ["http://swapi.co/api/people/1", "http://swapi.co/api/people/2", "http://swapi.co/api/people/3", "http://swapi.co/api/people/4", "http://swapi.co/api/people/5"];

	// Maps all of the links
	function getPeople() {
		return links.map(function (item, index) {
			return fetchJSON(item);
		});
	}

	// Filters through the links in search for the fifth link
	function filterArr() {
		return links.filter(function (item) {
			return item === "http://swapi.co/api/people/5";
		});
	}

	// testing fetchJSON
	var guy = fetchJSON(filterArr());
	guy.then(function (value) {
		console.log(value);
	});
	console.log(guy);

	var itemPromises = links.map(fetchJSON);

	// for (var i = itemPromises.length - 1; i >= 0; i--) {
	// 	console.log(itemPromises[i]);
	// 	itemPromises[i].then((value) => { console.log(value) });
	// }

	Promise.all(itemPromises).then(function (results) {
		// console.log(results);
		results.forEach(function (item) {
			console.log(item);
		});
	});
	//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9pbmRleC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTs7OztBQUlBLFNBQVMsU0FBVCxDQUFtQixJQUFuQixFQUF3QjtBQUN2QixHQUFFLElBQUYsQ0FBTztBQUNOLE9BQUssSUFEQztBQUVOLFlBQVU7QUFGSixFQUFQLEVBR0csSUFISCxDQUdRO0FBQUEsU0FDUCxRQUFRLEdBQVIsQ0FBWSxJQUFaLENBRE87QUFBQSxFQUhSO0FBTUE7OztBQUdELElBQUksWUFBWSxTQUFaLFNBQVksQ0FBUyxHQUFULEVBQWE7QUFDNUIsS0FBRyxJQUFJLFdBQUosS0FBb0IsS0FBdkIsRUFBNkI7QUFDNUIsT0FBSyxJQUFJLElBQUksSUFBSSxNQUFKLEdBQWEsQ0FBMUIsRUFBNkIsS0FBSyxDQUFsQyxFQUFxQyxHQUFyQyxFQUEwQztBQUN6QyxVQUFPLElBQUksT0FBSixDQUFZLFVBQUMsT0FBRCxFQUFVLE1BQVYsRUFBcUI7QUFDdkMsTUFBRSxPQUFGLENBQVUsSUFBSSxDQUFKLENBQVYsRUFDQyxJQURELENBQ00sVUFBQyxJQUFEO0FBQUEsWUFBVSxRQUFRLElBQVIsQ0FBVjtBQUFBLEtBRE4sRUFFQyxJQUZELENBRU0sVUFBQyxHQUFELEVBQU0sTUFBTixFQUFjLEdBQWQ7QUFBQSxZQUFzQixPQUFPLFNBQVMsSUFBSSxPQUFwQixDQUF0QjtBQUFBLEtBRk47QUFHQSxJQUpNLENBQVA7QUFLQTtBQUNELEVBUkQsTUFRTztBQUNOLFNBQU8sSUFBSSxPQUFKLENBQVksVUFBQyxPQUFELEVBQVUsTUFBVixFQUFxQjtBQUN2QyxLQUFFLE9BQUYsQ0FBVSxHQUFWLEVBQ0MsSUFERCxDQUNNLFVBQUMsSUFBRDtBQUFBLFdBQVUsUUFBUSxJQUFSLENBQVY7QUFBQSxJQUROLEVBRUMsSUFGRCxDQUVNLFVBQUMsR0FBRCxFQUFNLE1BQU4sRUFBYyxHQUFkO0FBQUEsV0FBc0IsT0FBTyxTQUFTLElBQUksT0FBcEIsQ0FBdEI7QUFBQSxJQUZOO0FBR0EsR0FKTSxDQUFQO0FBS0E7QUFDRCxDQWhCRDs7QUFrQkEsSUFBSSxRQUFRLENBQ1gsOEJBRFcsRUFFWCw4QkFGVyxFQUdYLDhCQUhXLEVBSVgsOEJBSlcsRUFLWCw4QkFMVyxDQUFaOzs7QUFTQSxTQUFTLFNBQVQsR0FBb0I7QUFDbkIsUUFBTyxNQUFNLEdBQU4sQ0FBVSxVQUFTLElBQVQsRUFBZSxLQUFmLEVBQXFCO0FBQ3JDLFNBQU8sVUFBVSxJQUFWLENBQVA7QUFDQSxFQUZNLENBQVA7QUFHQTs7O0FBR0QsU0FBUyxTQUFULEdBQW9CO0FBQ25CLFFBQU8sTUFBTSxNQUFOLENBQWEsVUFBUyxJQUFULEVBQWM7QUFDOUIsU0FBTyxTQUFTLDhCQUFoQjtBQUNILEVBRk0sQ0FBUDtBQUdBOzs7QUFHRCxJQUFJLE1BQU0sVUFBVSxXQUFWLENBQVY7QUFDQSxJQUFJLElBQUosQ0FBUyxVQUFDLEtBQUQsRUFBVztBQUFFLFNBQVEsR0FBUixDQUFZLEtBQVo7QUFBb0IsQ0FBMUM7QUFDQSxRQUFRLEdBQVIsQ0FBWSxHQUFaOztBQUVBLElBQUksZUFBZSxNQUFNLEdBQU4sQ0FBVSxTQUFWLENBQW5COzs7Ozs7O0FBT0EsUUFBUSxHQUFSLENBQVksWUFBWixFQUEwQixJQUExQixDQUErQixVQUFTLE9BQVQsRUFBaUI7O0FBRS9DLFNBQVEsT0FBUixDQUFnQixVQUFTLElBQVQsRUFBYztBQUM3QixVQUFRLEdBQVIsQ0FBWSxJQUFaO0FBQ0EsRUFGRDtBQUdBLENBTEQiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiL0FwcGxpY2F0aW9ucy9NQU1QL2h0ZG9jcy9mZWQvaHcyIiwic291cmNlc0NvbnRlbnQiOlsiJ3VzZSBzdHJpY3QnO1xuXG4vLyBTaW1wbGUgQUpBWCByZXF1ZXN0XG5cbmZ1bmN0aW9uIGdldFRoaW5ncyhsaW5rKXtcblx0JC5hamF4KHtcblx0XHR1cmw6IGxpbmssXG5cdFx0ZGF0YVR5cGU6IFwianNvblwiXG5cdH0pLmRvbmUoZGF0YSA9PiBcblx0XHRjb25zb2xlLmxvZyhkYXRhKVxuXHQpXG59XG5cbi8vIEpzb24gcmVxdWVzdHMgZm9yIGFycmF5cyBvciBzaW5nbGUgVVJMJ3NcbnZhciBmZXRjaEpTT04gPSBmdW5jdGlvbih1cmwpe1xuXHRpZih1cmwuY29uc3RydWN0b3IgPT09IEFycmF5KXtcblx0XHRmb3IgKHZhciBpID0gdXJsLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG5cdFx0XHRyZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuXHRcdFx0XHQkLmdldEpTT04odXJsW2ldKVxuXHRcdFx0XHQuZG9uZSgoanNvbikgPT4gcmVzb2x2ZShqc29uKSlcblx0XHRcdFx0LmZhaWwoKHhociwgc3RhdHVzLCBlcnIpID0+IHJlamVjdChzdGF0dXMgKyBlcnIubWVzc2FnZSkpO1xuXHRcdFx0fSk7XG5cdFx0fVxuXHR9IGVsc2Uge1xuXHRcdHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG5cdFx0XHQkLmdldEpTT04odXJsKVxuXHRcdFx0LmRvbmUoKGpzb24pID0+IHJlc29sdmUoanNvbikpXG5cdFx0XHQuZmFpbCgoeGhyLCBzdGF0dXMsIGVycikgPT4gcmVqZWN0KHN0YXR1cyArIGVyci5tZXNzYWdlKSk7XG5cdFx0fSk7XG5cdH1cdFxufVxuXG52YXIgbGlua3MgPSBbXG5cdFwiaHR0cDovL3N3YXBpLmNvL2FwaS9wZW9wbGUvMVwiLFxuXHRcImh0dHA6Ly9zd2FwaS5jby9hcGkvcGVvcGxlLzJcIixcblx0XCJodHRwOi8vc3dhcGkuY28vYXBpL3Blb3BsZS8zXCIsXG5cdFwiaHR0cDovL3N3YXBpLmNvL2FwaS9wZW9wbGUvNFwiLFxuXHRcImh0dHA6Ly9zd2FwaS5jby9hcGkvcGVvcGxlLzVcIlxuXTtcblxuLy8gTWFwcyBhbGwgb2YgdGhlIGxpbmtzXG5mdW5jdGlvbiBnZXRQZW9wbGUoKXtcblx0cmV0dXJuIGxpbmtzLm1hcChmdW5jdGlvbihpdGVtLCBpbmRleCl7XG5cdFx0cmV0dXJuIGZldGNoSlNPTihpdGVtKTtcblx0fSk7XG59XG5cbi8vIEZpbHRlcnMgdGhyb3VnaCB0aGUgbGlua3MgaW4gc2VhcmNoIGZvciB0aGUgZmlmdGggbGlua1xuZnVuY3Rpb24gZmlsdGVyQXJyKCl7XG5cdHJldHVybiBsaW5rcy5maWx0ZXIoZnVuY3Rpb24oaXRlbSl7XG4gICAgXHRyZXR1cm4gaXRlbSA9PT0gXCJodHRwOi8vc3dhcGkuY28vYXBpL3Blb3BsZS81XCI7XG5cdH0pO1xufVxuXG4vLyB0ZXN0aW5nIGZldGNoSlNPTlxudmFyIGd1eSA9IGZldGNoSlNPTihmaWx0ZXJBcnIoKSk7XG5ndXkudGhlbigodmFsdWUpID0+IHsgY29uc29sZS5sb2codmFsdWUpIH0pO1xuY29uc29sZS5sb2coZ3V5KTtcblxudmFyIGl0ZW1Qcm9taXNlcyA9IGxpbmtzLm1hcChmZXRjaEpTT04pO1xuXG4vLyBmb3IgKHZhciBpID0gaXRlbVByb21pc2VzLmxlbmd0aCAtIDE7IGkgPj0gMDsgaS0tKSB7XG4vLyBcdGNvbnNvbGUubG9nKGl0ZW1Qcm9taXNlc1tpXSk7XG4vLyBcdGl0ZW1Qcm9taXNlc1tpXS50aGVuKCh2YWx1ZSkgPT4geyBjb25zb2xlLmxvZyh2YWx1ZSkgfSk7XG4vLyB9XG5cblByb21pc2UuYWxsKGl0ZW1Qcm9taXNlcykudGhlbihmdW5jdGlvbihyZXN1bHRzKXtcblx0Ly8gY29uc29sZS5sb2cocmVzdWx0cyk7XG5cdHJlc3VsdHMuZm9yRWFjaChmdW5jdGlvbihpdGVtKXtcblx0XHRjb25zb2xlLmxvZyhpdGVtKTtcblx0fSk7XG59KSJdfQ==

/***/ }
/******/ ]);