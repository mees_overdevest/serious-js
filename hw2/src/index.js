'use strict';

// Simple AJAX request

function getThings(link){
	$.ajax({
		url: link,
		dataType: "json"
	}).done(data => 
		console.log(data)
	)
}

// Json requests for arrays or single URL's
var fetchJSON = function(url){
	if(url.constructor === Array){
		for (var i = url.length - 1; i >= 0; i--) {
			return new Promise((resolve, reject) => {
				$.getJSON(url[i])
				.done((json) => resolve(json))
				.fail((xhr, status, err) => reject(status + err.message));
			});
		}
	} else {
		return new Promise((resolve, reject) => {
			$.getJSON(url)
			.done((json) => resolve(json))
			.fail((xhr, status, err) => reject(status + err.message));
		});
	}	
}

var links = [
	"http://swapi.co/api/people/1",
	"http://swapi.co/api/people/2",
	"http://swapi.co/api/people/3",
	"http://swapi.co/api/people/4",
	"http://swapi.co/api/people/5"
];

// Maps all of the links
function getPeople(){
	return links.map(function(item, index){
		return fetchJSON(item);
	});
}

// Filters through the links in search for the fifth link
function filterArr(){
	return links.filter(function(item){
    	return item === "http://swapi.co/api/people/5";
	});
}

// testing fetchJSON
var guy = fetchJSON(filterArr());
guy.then((value) => { console.log(value) });
console.log(guy);

var itemPromises = links.map(fetchJSON);

// for (var i = itemPromises.length - 1; i >= 0; i--) {
// 	console.log(itemPromises[i]);
// 	itemPromises[i].then((value) => { console.log(value) });
// }

Promise.all(itemPromises).then(function(results){
	// console.log(results);
	results.forEach(function(item){
		console.log(item);
	});
})