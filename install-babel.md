# Installation guide to babel

We need babel to enable ES2015 code to work in different browsers.

1. First install node.
https://nodejs.org/en/
Check <code>node -v</code> and <code>npm -v</code> to verify your install (it should output version numbers)

2. Create a folder for your project
<code>mkdir yourProjectName</code> of do this within your created phpStorm project

3. Install babel locally
Run <code>npm init</code> 
It will initialize the project and create a package.json file. In this package all the dependencies on other libs are included.
Run <code>npm install babel-cli --save-dev</code>
It will install babel locally en add the dependency to the package.json file.

4. Create two directories within the project folder (or with phpStorm)
<code>mkdir dist</code> and 
<code>mkdir src</code>.

5. Install the es2015 presets
<code>npm install babel-preset-es2015 --save-dev</code>
The <code>--save-dev </code> will add the preset dependency to the package.json.

6. Use it to transpile the es2015 code.
<code>babel src --presets es2015 --out-dir dist</code>
This code will use the es2015 preset to take the js code in the src dir and transpile it to the dist dir.
<code>babel src --presets es2015 --out-file dist/bundle.js</code>
This code will use the es2015 preset and takes all of the .js code in the src dir and transpile it to one js file (named bundle) in the dist dir.
<code>babel src —presets es2015 —out-dir dist -s</code>
<code>babel src —presets es2015 —out-dir dist -w</code>

7. Add a babel configuration file
Step 7 is kind-a-tricky. You can set the preset up in a babel config file, in order to do so. create a <code>.babelrc</code> file.

Add the following content to the file.
<code>
{
  "presets": ["es2015"],
  "sourceMaps": "inline"
}
</code>
Now you just can use <code>babel src --out-dir dist</code> and it will take the presets from the babel configuration file.

8. Add babel scripts to your package.json for easier use (needed on Windows anyway because ./node_modules/.bin/babel doesn't work)
<code>
"scripts": {
  "babel:build:js": "babel src --out-file dist/bundle.js",
  "babel:watch:js": "babel src --watch --out-file dist/bundle.js"
},
</code>

After this you can just run <code>npm run babel:watch:js</code> to start your file watcher
