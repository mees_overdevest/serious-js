# Installing webpack

We need to install babel on top of babel, so we can use modules in the browser.

1. First install babel

   See babel-installation-guide.md
   Copy the latest package.json for this new 'project'

2. Install modules-commonjs
```cli
npm install babel-plugin-transform-es2015-modules-commonjs --save-dev
```

3. Add to `.babelrc`
```javascript
{
  "plugins": ["transform-es2015-modules-commonjs"],
}
```

4. Install webpack locally
```
npm install webpack —save-dev
```

5. Create webpack config

  1. install babel loader en babel core
  ```
  npm install babel-loader babel-core —save-dev
  ```

  2. Create a webpack.config.js file
  add the following to the config file.
  ```javascript
	module.exports = {
        entry: ‘./src/main.js',
        output: {
            path:'dist',
            filename:'bundle.js'
        },
        module:{
            loaders:[
                {test: /\.js$/, exclude:/node_modules/, loader:'babel-loader'}
            ]
        }
	}
  ```
6. Add a webpack script to package.json
```javascript
"webpack:build:js": "webpack",
"webpack:watch:js": "webpack --watch"
```
